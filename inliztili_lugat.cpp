            ++  Length-                                      uzunlik   
            ++  Compare-                                     taqqoslamoq
            ++  Western-                                     ģarbiy
            ++  Partly-                                      qisman
            ++  Own-                                         egasi
            ++  Feather-                                     pat
            ++  Hungry-                                     och qolmoq
            ++  Meat eater-                                 gòshtxòr
            ++  Hunter-                                     ovchi
            ++  Up to-                                      qadar
            ++  Carnivores-                                 yirtqich hayvonlar
            ++  Complete-                                   bajarmoq
            ++  Bones-                                      suyuk
            ++  Rex-                                        reks
            ++  Largest-                                    eng katta
            ++  Ever-                                       har doim
            ++  Grow to-                                    òsmoq
            ++  North-                                      shimol
            ++  Cover-                                      qoplamoq
            ++  Mouth-                                      oģiz
            ++  Teeth-                                      tishlar
            ++  Paid-                                       tòlamoq
                Mary likes to run ...
                7-matn 
            ++  Training-                                     mashģulot
            ++  Long distances -                              uzoq masofalar
            ++  Agreed -                                      kelishilgan
            ++  Let-                                          mayli
            ++  Classic -                                     klassik
            ++  Race -                                        poyga
            ++  Takes -                                       olmoq
            ++  Winner -                                      ģolib
            ++  Lifetime -                                    muddat
            ++  Supply -                                      ta'minot
            ++  Peanut -                                      yeryonģoq
            ++  Practised -                                   amalda
            ++  Average -                                     òrtacha
            ++  Ran faster -                                  tezroq yugurmoq
            ++  Fourth -                                      tòrtinchi
            ++  Pick up -                                     moshinada olib ketmoq
                Pace -                                        tezlik
                Reached -                                     yetmoq
               // Pure water lacks ..... /// hammasi yodlangan
               // 8-matn
            ++  Pure -                                      sof
                Lacks -                                     kamchiliklar
            ++  Smell -                                     hid
            ++  Survive-                                    omon qolmoq
            ++  Weight-                                     oģirligi
                Waste-                                      chiqindi
                Ancient-                                    qadimgi
            ++  Philosopher-                                faylasuf
            ++  Block-                                      bloklash
            ++  Liquid-                                     suyuqlik
                Measured-                                   òlchangan
            ++  Chemist-                                    kimyogar
            ++  Mixture-                                    aralash
                Consist-                                    òz ichiga olmoq
                Plenty-                                     kòp
              //  One day Nasreddin .....
              //  9-matn
                Borrow-                                     qarz olmoq
            ++  Neighbour-                                  qòshni
                Manners-                                    xulq atvor
                Brought-                                    olib kelmoq
                Pot-                                        qozon
            ++  Mine-                                       meniki
            ++  While-                                      vaholanki
            ++  Pleased-                                    mamnun
            ++  Lend-                                       qarz bermoq
            ++  Hoping-                                     umid qilmoq
            ++  Receive -                                   qabul qilmoq
            ++  Return -                                    qaytish
            ++  Still -                                     hali ham
                Patience -                                  sabr
                Demand -                                    talab
            ++  Property -                                  mulk
            ++  Screamed-                                   qichqirdi
            ++  Cinema-                                     kino
            ++  Animated-                                   jonlantirilgan
            ++  Movie-                                      film
            ++  At the end-                                 oxirida
            ++  Amazing-                                    ajoyib
            ++  Count-                                      sanamoq
            ++  Different kinds of-                         ...ning turli xili 
            ++  Insects-                                    hasharotlar
            ++  Surprise-                                 hayranlanish
            ++  Neck-                                       bòyin
            ++  Useful-                                     foydali
            ++  Tall-                                       uzun
            ++  Favourite-                                  sevimli
            ++  Fantastic-                                  ajoyib
            ++  Written by- ...                             tomonidan yozilgan
            ++  Worry                                       havotirlanmoq
            ++  Language-                                   til
            ++  Great-                                      buyuk
            ++  Fell happy-                                 xursand his qilmoq
            ++  Thank-                                    rahmat
            ++  Wonderful-                                  ajoyib
            ++  Again-                                      yana
            ++  Hundreds-                                   yuzlab
            ++  Sure-                                       ishonmoq
            ++  Invented                                    taklik qilingan
            ++  Believe-                                    ishonmoq
            ++  Century-                                    asr
            ++  Back -                                      qaytmoq
                Then                                        keyin
            ++  Shepherds-                                  chòpon
            ++  Looked after-                               ģamxòrlik qilmoq
                Bored-                                      zerikmoq
            ++  Push-                                       itarmoq
            ++  Rabbit-                                     quyon
            ++  Holes-                                      teshiklar
            ++  Sticks-                                     tayoqlar
                Soon-                                       tez orada
                Throughout-                                 butun bòylab
            ++  Even-                                       hatto
            ++  Century-                                    asr
                Introduce-                                 tanishtirmoq
            ++  Such as-                                    kabi
            ++  Enjoy-                                      rohatlanmoq
            ++  Natural-                                    tabiiy
            ++  Beauty-                                     chiroy
            ++  Fresh-                                      yangi toza sof
            ++  Relaxing-                                   rohatlanish
            ++  Crowded-                                    gavjum
                Swing-                                      tebranish
            ++  Strong-                                     kuchli
            ++  Healthy-                                    soģlom
            ++  Space-                                      fazo
//***********************************************topshirilgan *************************************************/
                                          Dear Malika.. 
                    ++    Just-                                      Shunchaki
                    ++    Fun-                                       qiziqarli
                    ++    Having-                                    ega
                    ++    Hardly-                                    zórga
                    ++    Tricycle-                                  3ģildiraklik velosipet 
                    ++    Advise-                                    maslahat 
                    ++    May-                                       mumkin
                    ++    Fall-                                      tushish
                    ++    Helmet-                                    tubulğa
                    ++    Cap-                                       qopqoq 
                    ++    Worn-                                      eskirgan
                    ++    Protect-                                   himoya qilish
                    ++    Bring-                                     olib kelgan
                    ++    Seaside-                                   dengiz qirģogi

                                       Some animals have parachute...
                    ++    Prachute-                                  parashut 
                    ++    Branch-                                    filial
                    ++    Lower-                                     pastroq 
                    ++    Spread-                                    tarqatish
                    ++    Wide-                                      keng 
                    ++    Apart-                                     alohida
                    ++    Acts-                                      harakat qiladi
                    ++    Tail-                                      quyruq
                    ++    Frog-                                      qurbaqa
                    ++    Toes-                                      oyoq barnoqlari
                    ++    Cannot-                                    mumkin emas
                    ++    Visible-                                   kòrinadigan 
                    ++    Stretches-                                 chòziladi            
                    ++    rest                                       dam olish
                                      Today i went to the cinema... 
                    ++    Cinema-                                   kino
                    ++    Animated-                                 jonlantirilgan
                    ++    Movie-                                    film
                    ++    At the end-                               oxirida
                    ++    Amazing-                                  ajoyib
                    ++    Count-                                    sanamoq
                    ++    Different kinds of-                       .....ning turli xili 
                    ++    Insects-                                  hasharotlar
                    ++    Surprising-                               hayranlanish
                    ++    Neck-                                     bòyin
                    ++    Useful-                                   foydali
                    ++    Tall-                                     uzun
                    ++    Favourite-                                sevimli
                    ++    Fantastic-                                ajoyib
                    ++    Written by-                               ... tomonidan yozilgan
                    ++    Worry                                     havotirlanmoq
                    ++    Language-                                 til
                    ++    Great-                                    buyuk
                    ++    Fell happy-                               xursand his qilmoq
                    ++    Thanked-                                  rahmat
                    ++    Wonderful-                                ajoyib
                    ++    Again-                                    yana

                                      Golf has been played...
                    ++    Hundreds-                                 yuzlab
                    ++    Sure-                                       ishonmoq
                    ++    Invented                                    taklik qilingan
                    ++    Believe-                                    ishonmoq
                    ++    Century-                                    asr
                    ++    Back -                                      qaytmoq
                    ++    Then                                        keyin
                    ++    Shepherds-                                  chòpon
                    ++    Looked after-                               ģamxòrlik qilmoq
                    ++    Bored-                                      zerikmoq
                    ++    Pushed-                                     itarmoq
                    ++    Rabbit-                                     quyon
                    ++    Holes-                                      teshiklar
                    ++    Sticks-                                     tayoqlar
                    ++    Soon-                                       tez orada
                    ++    Throughout-                                 butun bòylab
                    ++    Even-                                       hatto
                    ++    Century-                                    asr
                    ++    Introduced-                                 tanishtirmoq
                    ++    Such as-                                    kabi
                    ++    Enjoy-                                      rohatlanmoq
                    ++    Natural-                                    tabiiy
                    ++    Beauty-                                     chiroy
                    ++    Fresh-                                      yangi toza sof
                    ++    Relaxing-                                   rohatlanish
                    ++    Crowded-                                    gavjum
                        Swinging-                                   tebranish
                    ++    Strong-                                     kuchli
                    ++    Healthy-                                    soģlom
                    ++    Space-                                      fazo
                                        You probably think...
                    ++    Probably-                                   ehtinol
                    ++    Regular-                                    muntazam
                    ++    Invented-                                   taklif qilingan
                    ++    Stones-                                     toshlar
                    ++    Created-                                    yaratgan
                    ++    Difficult-                                  qiyin
                    ++    Space-                                      bòshliq , fazo
                    ++    Clearly-                                    aniq
                    ++    Important-                                  muhim
                                        Michail Joseph Jackson...
                    
                    ++    Legend-                                     afsona
                    ++    Successful-                                 muvaffaqiyatli
                    ++    Records-                                    rekordlar
                    ++    Awards-                                     mukofotlar
                    ++    Including-                                  jumladan , òz ichiga olmoq
                    ++    Singing-                                    kuylash
                    ++    Band-                                       guruj
                    ++    Quickly-                                    tez
                    ++    Popular-                                    mashur
                    ++    History-                                    tarix
                    ++    Nickname-                                   laqab
                    ++    Performances-                                 ijro
                    ++    Special effects-                            maxsus tasirlar
                    ++    Work-                                       ish
                    ++    Fans-                                       fanatlar
                    ++    Talented-                                   qobiliyatli

                                        Egypt ancient Great pyramids...
                    ++    Ancient-                                    tarixiy
                    ++    Imagine-                                    kòrinishi
                    ++    Including-                                  jumladan
                    ++    Paints                                      rasmlar
                    ++    Painters-                                   rassom
                    ++    Rules-                                      qoidalar
                    ++    Recognize-                                  tan olish
                    ++    Drew-                                       chizdi
                    ++    Best-looking angle-                         eng chiroyli farishta
                    ++    Drawn                                       chizilgan
                    ++    Shape-                                      shakl
                    ++    Nose-                                       burun
                    ++    Size-                                       òlcham
                    ++    Important-                                  muhim
                    ++    Described-                                  tasvirlangan
                    ++    God-                                        xudo
                    ++    Greater-                                    buyuk 

                                     Spam emails...       

                    ++    Tired-                                      charchagan
                    ++    Receive-                                    qabul qilmoq
                    ++    Without-                                    ...siz
                        Advertise-                                    reklama
                        Fill up-                                      tòldirish
                        Unfortunately-                                afsuski
                        Annoying-                                     bezovta qilish
                        Advertisement-                                reklama
                    ++    Dangerous-                                  xavfli
                    ++    Phishing-                                   shaxslarni parollar va kredit karta raqamlari kabi shaxsiy ma'lumotlarini oshkor qilishga undash uchun obroli kompaniyalardan ekanligi korsatilgan elektron pochta xabarlarini yuborishning firibgarlik amaliyoti.
                    ++    Link to-                                    .. ga havola 
                    ++    Usernames-                                  foydalanuvchi nomi
                    ++    Sender-                                     jònatuvchi
                    ++    Steal-                                        ògirlamoq
                        Infect-                                       yuqtirmoq

                                        Make the dinner 

                                                                
                    ++    Vegetables-                                 sabzavot
                    ++    Dinner-                                     kechki ovqat
                    ++    Perfect-                                    mukammal
                    ++    Instruments-                                asboblar
                    ++    Looking for-                                izlamoq
                    ++    Knife-                                      pichoq
                    ++    Flute-                                      nay
                    ++    Trumpet-                                    truba
                    ++    Pumpkin drum-                               qovoq baraban
                    ++    Audience-                                   tomoshabinlar
                    ++    Unique-                                     yagona
                    ++    Hall-                                       zal
                    ++    Delicious-                                  mazali
                    ++    Soup-                                       shòrva


                                In old times

                    almanac-                                           
                    chart-                                            diagramma              
                    movement-                                         harakat      
                    period-                                           davr
                    issue-                                            masala
                    prediction-                                       bashorat              
                    tradition-                                        anana
                    poems-                                            sher          
                    tale-                                             ertak   

                                some people keep
                    
                    round                                           dumaloq 
                    rectangular                                     4burchak
                    gravel                                          shagal
                    decorate                                        bezash
                    pump                                            nasos
                    
                                Substance can be 

                    substance 
                    solid 
                    depend 
                    vapour
                    steam
                    cloud
                    fall
                    amount
                    matter
                    noves
                    actually
                    trick


                                A shooting star is ...
                +    Shooting-                                   otishma
                +    Seeing-                                     kòrish
                +    Bring-                                      olib kelmoq
                +    Luck-                                       omad
                +    Disappear-                                  yòqolib ketmoq
                +    Correct-                                    tògri
                +    Meteor-
                +    besides-                                    bundan tashqari
                +    Large-                                      katta
                +    Rock-                                       rok
                +    Pulled-                                     tortildi
                +    Toward-
                +    Gravity-                                    gravitatsiya
                +    Surrounded-                                 òralgan
                +    Night-                                      kecha
                +    Turn-                                       burilish

                                With wings that can ...
                +    Wings-                                      qanotlar
                +    Condor-                                     kondor
                +    Huge-                                       katta
                +    Pattern-                                    naqsh
                +    Feather-                                    tuklar
                +    Scavenger-                                  chiqindichi
                +    Vulture-                                    tuxum
                +    Rare-                                       kamdan
                +    Endangered-                                 xavf ostida
                +    Dead animal-                                òlik hayvon
                +    Feed-                                       oziqlantirish
                +    Lead-                                       qòrģoshin
                +    Wild-                                       yovvoyi

                                Ice cream has been ..
                +    Available-                                  mavjud
                +    Freezer-                                    muzlatgich
                +    Fridge-                                     sovutgich
                +    Salesman-                                   sotuvchi
                +    Thus-                                       shunday qilib
                +    Waffle-                                     vafli
                +    Sunday-                                     yakshanba
                +    Flavour-                                    lazzat


                +    1.conveniet-                                qulay
                +    Cans-                                       bankalar
                +    Considered-                                 kòrib chiqildi
                +    Contain-                                    tarkibida
                +    Similarly-                                  xuddi shunday
                +    Insted of-                                  orniga
                +    Frying-                                     qovurish
                +    Raw-                                        xom

                +    lecture-                                    leksiya
                +    Spoken-                                     gapirish
                +    Amusing-                                    qiziqarli
                +    Lasted-                                     davom etdi
                +    At last-                                    nihoyat
                +    Few seconds-                                bir necha soniya
                +    Laughed loudly-                             qattiq kulish
                +    Respectful-                                 hurmatli
                +    Invited-                                    taklif qilmoq
                +    Interpret-                                  tarjimon
                +    Laugh-                                      kulmoq

                +    3.invention-                                kashfiyot
                +    Thought-                                    fikr
                +    Respect-                                    hurmatysh
                +    Suggestion-                                 taklif
                +    Turn off-                                   òchirib qòyish
                +    Shut off-                                   òchirish
                +    Perhaps-                                    balki
                +    Fully-                                      tòliq
                +    Even for-                                   hattouchun
                +    Led(lead)-                                  qòrgoshin
                +    Complete                                    bajarildi
                +    Decided-                                    qaror qilmoq
                +    Impossible                                  mumkin emas
                +    Lower-                                      astroq
                +    In this way-                                shu tarzda
                +    Else to-                                    shunday qilib
                +    Countrymen s-                               yurtdoshlar

                +    4.hunting-                                  ov qilish
                +    Practice-                                   amaliyot
                +    Trouble-                                    muammo
                +    Glasses-                                    kòzoynak
                +    Rained-                                     yomģir yoģdi
                +    Wet-                                        nam
                +    Go out-                                     tashqariga chiqmoq
                +    Rainy-                                      yomģir
                +    Unfortunately-                              afsuski
                +    Injured-                                    jaralangan
                +    Judge-                                      hakam
                +    Wrong-                                      notòģri
                +    Back at-                                    orqaga

                +    5.tattoos-                                  tatuirovka
                +    In fact-                                    aslida
                +    Archaeologists-                             arxeologlar
                +    Ankle-
                +    Knees-                                      tizzalar
                +    Ancient-                                    qadimgi
                +    Criminal-                                   jinoyatchi
                +    Prisoner-                                   mahbus
                +    Religious-                                  diniy
                +    Sailor-                                     dengizchi
                +    Shoulder-                                   yelka
                +    Chest-                                      kòkrak
                +    Back-                                       orqaga
                +    Described-                                  tasvirlamoq
                    Feature-                                    xususiyat

                    6.spacecraft-                               kosmik kema
                +    Took off-                                   uchib ketmoq
                +    Journey-                                   sayohat
                +    Arrived-                                   yetib kelmoq
                +    Planet-                                    sayyora
                +    Solar system-                              quyosh sistemasi
                +    Cruel-                                     shafqatsiz
                +    Volcanoes-                                 vulqonlar
                +    Rings-                                     uzuklar
                +    Continued                                  davomi
                +    Surface-                                   yuza
                +    Round-                                     umaloq
                +    Uranus-                                    uran
                +    Eventually-                                oxir oqibat
                +    Fixed-                                     tugallamoq
                +    Mechanical-                                mexanik
                +    Pluto-                                     pluton
                +    Ankle-                                     tòpiq
                                        




