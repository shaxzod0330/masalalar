#include <iostream>
#include <stack>
#include <string>

using namespace std;

int main() {
    string s;
    getline(cin, s);
    stack<char> st;
    
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == '(' || s[i] == '{' || s[i] == '[') {
            st.push(s[i]);
        } else if (s[i] == ')') {
            if (st.empty() || st.top() != '(') {
                cout << i << endl;
                return 0;
            }
            st.pop();
        } else if (s[i] == '}') {
            if (st.empty() || st.top() != '{') {
                cout << i << endl;
                return 0;
            }
            st.pop();
        } else if (s[i] == ']') {
            if (st.empty() || st.top() != '[') {
                cout << i << endl;
                return 0;
            }
            st.pop();
        }
    }
    
    if (st.empty()) {
        cout << 0 << endl;
    } else {
        cout << -1 << endl;
    }
    
    return 0;
}// 70 chatGPT qilgan kod 