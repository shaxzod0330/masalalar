#include <bits/stdc++.h>
using namespace std;
void Chessboard(int m,int n){
    int b[m][n]; 
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            if((i+j)%2==0){
                b[i][j]=1;
            }
            else {
                b[i][j]=0;
            }
        }
    }
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            cout<<b[i][j]<<" ";
        }
        cout<<endl;
    }
}
int main(){
    int k,m,n;
    cin>>m>>n;
    Chessboard(m,n);
}//18